//Определяем размеры всех блоков, и пересчитываем общую ширину
$(window).load(function(){
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        mobile();
    } else {
        heightCalc();
        setSlideSize();
        heightCalc();
        slideDocument();
    }

});

//События при изменении размера экрана
$(window).on('resize', function(){
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        mobile();
    } else {
        heightCalc();
    }
});


// Блок высчитывающий высоту экрана, и на основе этого делающих скролин либо вертикальным
// либо горизонтальным



function heightCalc(){

    if($(window).width() > 960){
        desctop();
    } else {
        mobile();
    }

}


function desctop(){
    var slides = $('.b-slide-item'),
        clientHeigth = $(window).height(), //Высота экрана браузера
        slidesCss = {
            'min-height' : ''+clientHeigth+'',
            'height' : ''+clientHeigth+'',
            'float' : 'left'
        };

    slides.css(slidesCss);
    $('.full-height').css('height', ''+clientHeigth+'px');

    //Высчитываем размер блока b-slide-track (суммы ширины всех слайдов)
    var fullWidth = +1; // !px нужен для костыльного интернет эксплорера
    slides.each(function(){
        fullWidth += $(this).width();
        return fullWidth;

    });
    $('.b-slide-track').width(''+fullWidth+'');

}

//События по скролу

function slideDocument(){
    var nowPosition = 0;

    $(document).on('mousewheel', function(event, delta){
        var slideTrack = $('.b-slide-track'),
            endScroll = ($('.b-slide-track').width()- $(window).width())*(-1);
        //Отсеживаем куда крутилось колесико
        console.log(delta);
        if (delta > 0){
            nowPosition += delta*50;
            if(nowPosition >= 0){
                nowPosition = 0;
            }
        } else {
            nowPosition += delta*50;
            if(nowPosition <  endScroll){
                nowPosition = endScroll;
            }
        }



        slideTrack.stop().animate({ marginLeft : ''+nowPosition+'px'}, 1000, 'linear' );
        //slideTrack.animate({'transform': 'translate('+nowPosition+'px,0)'}, 'slow');
        //slideTrack.animate({'-moz-transform': 'translate('+nowPosition+'px,0)'}, 'slow');
        //slideTrack.animate({'-o-transform': 'translate('+nowPosition+'px,0)'}, 'slow');
        //slideTrack.animate({'-o-transform': 'translate('+nowPosition+'px,0)'}, 'slow');
        slideTrackEvent(nowPosition, event, delta);
        return nowPosition;
    });


}

function setSlideSize() {
    slide1Size();
    scroll3Size();
    scroll3Size();
    scroll4Size();
    scroll5Size();
    scroll6Size();
    scroll7Size();
    scroll8Size();
    scroll9Size();
    scroll10Size();
    portfolioSize();
    scroll12Size();
}
//
//Скрипты для Скролл 1
//

    function slide1Size(){
        var width = document.body.clientWidth - $('.b-slide-1__col:first').width();

        $('.b-slide-1__col:last').width(width);

        $('.b-slide-2__col:last').width(width);
    }


//
//Скрипты для Скролл 3
//

    function scroll3Size() {
        $('.slide3').width(document.body.clientWidth);
    }

//
//Скрипты для Скролл 4
//

    function scroll4Size() {
        $('.slide4').width(document.body.clientWidth);
    }


//
//Скрипты для Скролл 5 (Велосипед)
//
    function scroll5Size() {
        $('.slide5').width(document.body.clientWidth * 1.2);

        $('.b-sl-5-img').height(document.body.clientHeight * 0.595);
    }


//
//Скрипты для Скролл 6 (Еда)
//
    function scroll6Size() {
        $('.slide6').width(document.body.clientWidth);

        $('.b-slide-6-h3-img').height(document.body.clientHeight);

    }

//
//Скрипты для Скролл 7 (Продукты)
//

    function scroll7Size() {
        $('.slide7').width(document.body.clientWidth * 1.1);

        $('.b-slide-7__img').height(document.body.clientHeight);
        $('.b-slide-7__content').height(document.body.clientHeight);
    }

//
//Скрипты для Скролл 8
//

    function scroll8Size() {
        $('.slide8').height(document.body.clientHeight);
        $('.slide8').width(document.body.clientWidth * 3);
        $('.b-slide-8__section').width(document.body.clientWidth);
        $('.b-slide-8__section').height(document.body.clientHeight);
        $('.b-slide-8__bg').height(document.body.clientHeight * 0.5);
        $('.b-slide-8__bg').width(document.body.clientWidth * 4);
    }

//
//Скрипты для Скролл 9
//

    function scroll9Size() {
        $('.slide9').height(document.body.clientHeight);
        $('.slide9').width(document.body.clientWidth);
    }


//
//Скрипты для Скролл 10
//


    function scroll10Size() {
        $('.slide10').height(document.body.clientHeight);
        $('.slide10').width(document.body.clientWidth);
    }

//
//Скрипты для Скролл 11
//


    function portfolioSize() {
        $('.b-portfolio-img').height(document.body.clientHeight);

        var fullWidth = 0;

        $('.b-portfolio-img').each(function () {
            fullWidth += $(this).width();
            return fullWidth + 10;
        });

        $('.slide11').width('' + fullWidth + '');
    }

//
//Скрипты для Скролл 12
//

    function scroll12Size() {
        $('.slide12').height(document.body.clientHeight);
        $('.slide12').width(document.body.clientWidth );
    }
    


//
//  Запускаем различные скрипты в зависимости от того, сколько пользователь прокрутил
//

function slideTrackEvent(position, event, delta){
    position *= (-1);

    var slide1Width = $('.slide1').width(),
        slide2Width = $('.slide2').width(),
        slide3Width = $('.slide3').width(),
        slide4Width = $('.slide4').width(),
        slide5Width = $('.slide5').width(),
        slide6Width = $('.slide6').width(),
        slide7Width = $('.slide7').width(),
        slide8Width = $('.slide8').width();



    //Паралакс эффект в айфонах (2-й скролл)
    if (position > slide1Width - slide2Width * 0.7 && position < slide1Width + slide2Width - slide2Width * 0.7){
        var fase = $('.b-iphone-face').css('left');
        fase = parseFloat(fase);
        if(delta < 0){
            fase += delta * 70;

            $('.b-iphone-face').stop().animate({left: ''+fase+'px'},  1000, 'linear');
        } else {
            fase += delta * 70;
            $('.b-iphone-face').stop().animate({left: ''+fase+'px'},  1000, 'linear');
        }
    }


    //Выдвигающийся лабиринт из логотипа RADAR (3-й скролл)
    if (position > slide1Width + slide2Width* 0.8 && position < slide1Width + slide2Width + slide2Width* 0.1){
        $('.b-sl3-img__left').css('margin-right', '-130px');
    } else {
        $('.b-sl3-img__left').css('margin-right', '-253px');
    }
    //Тормозим велосипед на экране (5-й скролл "велосипед")

    var text = $('.b-sl-5-img'),
        nowPositionText = text.attr('data-transform');
    nowPositionText = parseInt(nowPositionText);

    if(position > slide1Width + slide2Width + slide3Width + slide4Width
        && position < slide1Width + slide2Width + slide3Width + slide4Width + slide5Width*0.3){

        if (delta > 0){
            nowPositionText -= delta * 50;

        } else {
            nowPositionText -= delta * 50;

        }
        text.stop().animate({left: ''+nowPositionText+'px'},  1000, 'linear');
        text.attr('data-transform', ''+nowPositionText+'');
    } else {
        text.stop().animate({left: ''+nowPositionText+'px'},  1000, 'linear');
        text.attr('data-transform', ''+nowPositionText+'');
    }

    //обнуляем положение

    if(position > 0
        && position < slide1Width + slide2Width + slide3Width + slide4Width){
        text.attr('data-transform', '0');
    }

    //Фиксируем фон (8-й скрол)
    var bg = $('.b-slide-8__bg'),
        bgPosition = bg.attr('data-transform');
    bgPosition = parseInt(bgPosition);

    if(position > slide1Width + slide2Width + slide3Width + slide4Width +
        slide5Width + slide6Width + slide7Width + slide7Width * 0.1 && position < slide1Width + slide2Width + slide3Width + slide4Width +
        slide5Width + slide6Width + slide7Width + slide8Width - slide8Width * 0.25 ){

        if (delta > 0){
            bgPosition -= delta * 50;
        } else {
            bgPosition -= delta * 50;
        }
        bg.attr('data-transform', ''+bgPosition+'');
        bg.stop().animate({left: ''+bgPosition+'px'},  1000, 'linear');

    } else {
        bg.attr('data-transform', ''+bgPosition+'');
        bg.stop().animate({left: ''+bgPosition+'px'},  1000, 'linear');

    }
    //обнуляем положение

    if(position < slide1Width + slide2Width + slide3Width + slide4Width +
        slide5Width + slide6Width + slide7Width
        || position > slide1Width + slide2Width + slide3Width + slide4Width +
        slide5Width + slide6Width + slide7Width + slide8Width){
        bg.stop(true, true).animate({left: '-200px'},  0);
        bg.attr('data-transform', '0');

    }

    //Вытаскиваем диск из коробочки

    if(position > slide1Width + slide2Width + slide3Width + slide4Width +
        slide5Width + slide6Width + slide7Width +  slide8Width + slide8Width * 0.25
        && position < slide1Width + slide2Width + slide3Width + slide4Width +
        slide5Width + slide6Width + slide7Width +  slide8Width + slide8Width * 0.35){

        $('.b-scroll-10-disk').css('margin-left', '-77px');

    } else {
        $('.b-scroll-10-disk').css('margin-left', '-290px');
    }
}






function mobile(){
    /*
    var slides = $('.b-slide-item'),
        clientHeigth = $(window).height(), //Высота экрана браузера
        clientWidth = $(window).width(),
        slidesCss = {
            'min-width' : ''+clientWidth+'',
            'width' : ''+clientWidth+'',
            'min-height' : ''+clientHeigth+'',
            'height' : '100%'

        };

    slides.css(slidesCss);

    */
}