//////////////////////////////////////////////////////
//Функции контроля
//////////////////////////////////////////////////////

window.onload = function(){
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        mobile();
    } else {
        heightCalc();
        setSlideSize();
        heightCalc();
    }
};

//События при изменении размера экрана
$(window).on('resize', function(){
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        mobile();
    } else {
        heightCalc();
    }
});



function heightCalc(){
    if($(window).width() > 960){
        desctop();
    } else {
        mobile();
    }
}


//
//Скрипты десктопной версии сайта
//

function desctop(){

    $('#mobile-style').remove();
    setSlideSize();
    reCountSmoothDivScroll();
    hidePreloader();
    scrollEvents();

}

//Высчитываем размеры каждого блока


function setSlideSize() {
    slide1Size();
    scroll3Size();
    scroll3Size();
    scroll4Size();
    scroll5Size();
    scroll6Size();
    scroll7Size();
    scroll8Size();
    scroll9Size();
    scroll10Size();
    portfolioSize();
    scroll12Size();
}

//Пересчитываем размер блока библиоткеи

function reCountSmoothDivScroll(){
    //$(document).resize();

    var fullWidth = 0;
    $('.b-slide-item').each(function(){
       fullWidth += $(this).width();
    });
    $('.scrollableArea').width(fullWidth);
}


//Скрипты меню
$(document).ready(
    function(){
        showMenu();
    }
);
function showMenu(){
    $('.b-menu__item').hover(
        function(){
            $(this).find('.b-menu-description').stop().animate({height: 'show'}, 300);

        },
        function(){
            $(this).find('.b-menu-description').stop().animate({height: 'hide'}, 300);
        }
    )

}

//Скрываем прелоадер

function hidePreloader(){
    $('.preloader').hide();
    $('#preloader-style').remove();
    $('.b-slide-track').css('opacity', '1')
}




//
//Скрипты мобильной версии сайта
//

function mobile(){

}


//////////////////////////////////////////////////////
//Функции выполнения
//////////////////////////////////////////////////////

//Размеры для Скролл 1

function slide1Size(){
    var width = document.body.clientWidth - $('.b-slide-1__col:first').width();

    $('.b-slide-1__col:last').width(width);

    $('.b-slide-2__col:last').width(width);
    $('.full-height').height(document.body.clientHeight);
}

//Размеры для Скролл 3

function scroll3Size() {
    $('#slide3').width(document.body.clientWidth);
    $('#slide3').height(document.body.clientHeight);

}

//Размеры для Скролл 4

function scroll4Size() {
    $('#slide4').width(document.body.clientWidth);
}


//Размеры для Скролл 5 (Велосипед)

function scroll5Size() {
    $('#slide5').width(document.body.clientWidth * 1.2);

    $('.b-sl-5-img').height(document.body.clientHeight * 0.595);
    $('.b-slide-5__row--height-fix').height($('.b-sl-5-img').height())
}

//Размеры для Скролл 6 (Еда)

function scroll6Size() {
    $('#slide6').width(document.body.clientWidth);

    $('.b-slide-6-h3-img').height(document.body.clientHeight);

}

//Размеры для Скролл 7 (Продукты)

function scroll7Size() {
    $('#slide7').width(document.body.clientWidth * 1.1);

    $('.b-slide-7__img').height(document.body.clientHeight);
    $('.b-slide-7__content').height(document.body.clientHeight);
}

//Размеры для Скролл 8

function scroll8Size() {
    $('#slide8').height(document.body.clientHeight);
    $('#slide8').width(document.body.clientWidth * 3);
    $('.b-slide-8__section').width(document.body.clientWidth);
    $('.b-slide-8__section').height(document.body.clientHeight);
    $('.b-slide-8__bg').height(document.body.clientHeight * 0.5);
    $('.b-slide-8__bg').width(document.body.clientWidth * 4);
}

//Размеры для Скролл 9

function scroll9Size() {
    $('#slide9').height(document.body.clientHeight);
    $('#slide9').width(document.body.clientWidth);
}


//Размеры для Скролл 10


function scroll10Size() {
    $('#slide10').height(document.body.clientHeight);
    $('#slide10').width(document.body.clientWidth);
}

//Размеры для Скролл 11


function portfolioSize() {
    $('.b-portfolio-img').height(document.body.clientHeight);

    var fullWidth = 0;

    $('.b-portfolio-img').each(function () {
        fullWidth += $(this).width();
        return fullWidth + 10;
    });

    console.log($('#slide9').width());
    $('#slide11').width('' + fullWidth + '');
}

//Размеры для Скролл 12

function scroll12Size() {
    $('#slide12').height(document.body.clientHeight);
    $('#slide12').width(document.body.clientWidth );
}


//
//События по скролу
//


function scrollEvents(){
    var widthSc1 = $('#slide1').width();
    var widthSc2 = $('#slide2').width();
    var widthSc3 = $('#slide3').width();
    var widthSc4 = $('#slide4').width();
    var widthSc5 = $('#slide5').width();
    var widthSc6 = $('#slide6').width();
    var widthSc7 = $('#slide7').width();
    var widthSc8 = $('#slide8').width();
    var widthSc9 = $('#slide91').width();
    var widthSc10 = $('#slide10').width();
    var count = 0;
    var countBg = 0;
    $(document).on('mousewheel', function(event, delta){

        var scroll = $('.scrollableArea').offset().left*(-1);

        if(scroll > widthSc1*0.3  && scroll < widthSc1 + widthSc2 ){

            var fase = $('.b-iphone-face').css('left');
            fase = parseFloat(fase);
            if(delta < 0){
                fase += delta * 70;

                $('.b-iphone-face').stop().animate({left: ''+fase+'px'},  320, 'linear');
            } else {
                fase += delta * 70;
                $('.b-iphone-face').stop().animate({left: ''+fase+'px'},  320, 'linear');
            }

        }

        if(scroll > widthSc1 + widthSc2 - widthSc2*0.3 && scroll < widthSc1 + widthSc2 + widthSc3*0.3){

            $('.b-sl3-img__left').css('margin-right', '-130px');



        } else {
            $('.b-sl3-img__left').css('margin-right', '-253px');
        }


        var text = $('.b-sl-5-img'),
            nowPositionText = text.attr('data-transform');
        nowPositionText = parseInt(nowPositionText);




        if(scroll > widthSc1 + widthSc2 + widthSc3 + widthSc4
            && scroll < widthSc1 + widthSc2 + widthSc3 + widthSc4 + widthSc5*0.2) {
            $('.b-sl-5-img').css('position','fixed');
            $('.b-sl-5-img').css('left',''+nowPositionText+'');
            count += delta;

            text.attr('data-transform', ''+nowPositionText+'');
        } else {
            $('.b-sl-5-img').css('position', 'relative');
            $('.b-sl-5-img').css('left', ''+count*70*(-1)+'');
            text.attr('data-transform', ''+nowPositionText+'');
        }

        var bg = $('.b-slide-8__bg');
        var bgPosition = bg.attr('data-transform');
        bgPosition = parseInt(bgPosition);

        if(scroll > widthSc1 + widthSc2 + widthSc3 + widthSc4
            + widthSc5 + widthSc6 + widthSc7
            && scroll < widthSc1 + widthSc2 + widthSc3 + widthSc4
            + widthSc5 + widthSc6 + widthSc7 + widthSc8){

            bg.css('position','fixed');
            bg.css('left',''+nowPositionText+'');
            count += delta;

        } else{
            bg.css('position', 'absolute');
            bg.css('left', ''+countBg*70*(-1)+'');
            text.attr('data-transform', ''+bgPosition+'');
        }


        if(scroll > widthSc1 + widthSc2 + widthSc3 + widthSc4
            + widthSc5 + widthSc6 + widthSc7 + widthSc8
            && scroll <   widthSc1 + widthSc2 + widthSc3 + widthSc4
            + widthSc5 + widthSc6 + widthSc7 + widthSc8 + widthSc9*0.8) {

            console.log('Зажги свет');
            $('#slide91').css('background', 'url(img/scroll-9-bg.jpg) 50% 50% no-repeat');
            $('.b-scroll-9-img').attr('src', 'img/scroll-9-lampa.png')
        } else {
            $('#slide91').css('background', 'url(img/scroll-9-bg-night.jpg) 50% 50% no-repeat');
            $('.b-scroll-9-img').attr('src', 'img/scroll-9-lampa-night.png')
        }


        if(scroll > widthSc1 + widthSc2 + widthSc3 + widthSc4
            + widthSc5 + widthSc6 + widthSc7 + widthSc8 + widthSc9*0.5
            &&  scroll < widthSc1 + widthSc2 + widthSc3 + widthSc4
            + widthSc5 + widthSc6 + widthSc7 + widthSc8 + widthSc9 + widthSc10*0.4 ){

            console.log(';bcr');
            $('.b-scroll-10-disk').css('margin-left', '-77px');

        } else {
            $('.b-scroll-10-disk').css('margin-left', '-290px');
        }

    });
}